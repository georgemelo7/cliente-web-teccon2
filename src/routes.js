import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views principais
import DashboardView from './components/dash/Dashboard.vue'
import TablesView from './components/dash/Tables.vue'
import TasksView from './components/dash/Tasks.vue'
import SettingView from './components/dash/Setting.vue'
import AccessView from './components/dash/Access.vue'
import ServerView from './components/dash/Server.vue'
import ReposView from './components/dash/Repos.vue'

// import viwes secundárias

import ListaUsuarios from './components/dash/usuario/lista_usuario.vue'
import NovoUsuario from './components/dash/usuario/novo_usuario.vue'
import EditarUsuario from './components/dash/usuario/editar_usuario.vue'
import Usuario from './components/dash/usuario/usuario.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  }, {
    path: '/',
    component: DashView,
    children: [
      {
        path: '',
        component: DashboardView,
        name: 'Dashboard',
        description: 'Overview of environment',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/tables',
        component: TablesView,
        name: 'Tables',
        description: 'Simple and advance table in CoPilot',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/tasks',
        component: TasksView,
        name: 'Tasks',
        description: 'Tasks page in the form of a timeline',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/setting',
        component: SettingView,
        name: 'Settings',
        description: 'User settings page',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/access',
        component: AccessView,
        name: 'Access',
        description: 'Example of using maps',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/server',
        component: ServerView,
        name: 'Servers',
        description: 'List of our servers',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/repos',
        component: ReposView,
        name: 'Repository',
        description: 'List of popular javascript repos',
        meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
      }, {
        path: '/usuarios',
        component: ListaUsuarios,
        name: 'Usuarios',
        description: 'Lista de usuarios',
        meta: { requiresAuth: true, permissao: ['admin'] },
        children: [
          {
            path: 'novousuario',
            component: NovoUsuario,
            name: 'NovoUsuario',
            description: 'Formulario de novo usuário',
            meta: { requiresAuth: true, permissao: ['admin'] }
          },
          {
            path: 'editarusuario',
            component: EditarUsuario,
            name: 'EditarUsuario',
            description: 'Formulario de edição de usuário',
            meta: { requiresAuth: true, permissao: ['admin'] }
          },
          {
            path: 'verusuario',
            component: Usuario,
            name: 'Usuario',
            description: 'Resumo de usuario',
            meta: { requiresAuth: true, permissao: ['admin'] }
          }
        ]
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
