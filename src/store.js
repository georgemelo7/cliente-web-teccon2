import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  urlAPI: 'http://localhost:8000/',
  authenticated: false,
  user: null,
  userInfo: {
    messages: [{1: 'test', 2: 'test'}],
    notifications: [],
    tasks: []
  },
  erroAcesso: false,
  usuarios: []
}

const mutations = {
  SET_USER (state, user) {
    state.user = user
  },
  ALTERAR_AUTENTIFICACAO (state, payload) {
    state.authenticated = payload.auth
  },
  ALTERAR_ERROACESSO (state, payload) {
    state.erroAcesso = payload.erro
  },
  ATUALIZAR_USUARIOS (state, payload) {
    state.usuarios = payload
  },
  ATUALIZAR_USUARIO (state, payload) {
    state.usuarios[payload.index].login = payload.login
    state.usuarios[payload.index].credential = payload.credential
    state.usuarios[payload.index].name = payload.name
  },
  ADD_USUARIO (state, payload) {
    state.usuarios.push(payload)
  }
}

export default new Vuex.Store({
  state,
  mutations
})
